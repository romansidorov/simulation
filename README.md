![Generated fixtures](http://dl4.joxi.net/drive/2022/03/28/0048/0324/3166532/32/88a9d023a7.jpg "Generated fixtures")

## Migrations and seeding

Just run ```php artisan migrate --seed``` and teams will be seeded into the database. You are able to choose different teams in ```database/seeders/TeamSeeder.php``` (I've prepared some).

## Running the app

Just go to the index page and you will see the teams list.

## Services to support the app

I've created several service-classes to handle the app data (they are placed in ```App\Services```):

- ```class League``` (is available through the facade ```League```) – league-wide data management (generating fixtures, total reset, playing all weeks' matches, retrieving the league matches)
- ```class MatchPlayer``` – a class to play a given match
- ```class TeamPower``` – a class to calculate team power points before a given match
- ```class Leaderboard``` – a wrapper class to handle teams scores after a given week
- ```class Predictor``` – a class to calculate teams odds after a given week