<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Team, Match, Matchweek, TeamScore};
use App\Facades\League;
use App\Http\Requests\MatchUpdateRequest;
use App\Events\MatchUpdated;

class AppController extends Controller
{
    /**
     * Show teams list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function teams()
    {
        # Retrieve all teams of the league
        $teams = League::teams();

        return view('teams', compact('teams'));
    }

    /**
     * Generate fixtures and redirect to the fixtures page.
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function generateFixtures()
    {
        League::generateFixtures();

        return redirect()
            ->route('fixtures')
            ->with('status', 'Fixtures have been generated.');
    }

    /**
     * Show generated fixtures.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fixtures()
    {
        # Retrieve all teams of the league
        $teams = League::teams();

        $matchweeks = Matchweek::query()
            ->with('matches')
            ->get();

        return view('fixtures', compact('matchweeks', 'teams'));
    }

    /**
     * Show simulation page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function simulation()
    {
        # Retrieve all teams of the league
        $teams = League::teams();

        $matches = Matchweek::query()
            ->first()
            ->matches;

        return view('simulation', compact('teams', 'matches'));
    }

    /**
     * Show a matchweek page.
     *
     * @param Matchweek $matchweek
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function matchweek(Matchweek $matchweek)
    {
        # Get matches for the matchweek
        $matches = $matchweek->matches;

        # Retrieve all teams of the league
        $teams = League::teams();

        # Get teams scores for the matchweek
        $teamsScores = TeamScore::getMatchweekScores($matchweek);

        return view('matchweek', compact('teamsScores', 'matches', 'matchweek', 'teams'));
    }

    /**
     * Reset all data
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset()
    {
        League::reset();

        return redirect()
            ->route('teams')
            ->with('status', 'All data has been reset.');
    }

    /**
     * Play next week
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function playNextWeek()
    {
        # Retrieve next matchweek
        $nextMatchweek = Matchweek::query()
            ->isNotPlayed()
            ->first();

        if (null === $nextMatchweek) {
            $lastMatchweek = Matchweek::query()
                ->get()
                ->last();

            return redirect()
                ->route('matchweek', $lastMatchweek)
                ->with('status', 'All weeks already played.');
        }

        # Play the matchweek
        $nextMatchweek->play();

        return redirect()
            ->route('matchweek', $nextMatchweek)
            ->with('status', 'Week played.');
    }

    /**
     * Play all weeks
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function playAllWeeks()
    {
        League::playAllMatchweeks();

        return redirect()
            ->route('fixtures', ['all'])
            ->with('status', 'All weeks played.');
    }

    /**
     * Update the given match
     */
    public function matchUpdate(Match $match, MatchUpdateRequest $request): \Illuminate\Http\JsonResponse
    {
        $match->update($request->validated());

        event(new MatchUpdated($match));

        return response()->json([
            'match' => $match
        ], 200);
    }

    /**
     * Get the given matchweek scores
     */
    public function matchweekScores(Matchweek $matchweek): \Illuminate\Http\JsonResponse
    {
        # Get teams scores for the matchweek
        $teamsScores = TeamScore::getMatchweekScores($matchweek);

        return response()->json([
            'scores' => $teamsScores->values(),
        ], 200);
    }
}
