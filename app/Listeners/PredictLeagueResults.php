<?php

namespace App\Listeners;

use App\Events\MatchweekPlayed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\Predictor;
use App\Models\{Matchweek, TeamScore};

class PredictLeagueResults
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\MatchweekPlayed  $event
     * @return void
     */
    public function handle(MatchweekPlayed $event)
    {
        /**
         * Checking if enough weeks have passed
         * to start the predictions
         */
        $matchweeksCount = Matchweek::count();
        $providedMatchweekNumber = $event->matchweek->getKey();
        $matchweeksRemainingNumber = $matchweeksCount - $providedMatchweekNumber;

        if ($matchweeksRemainingNumber >= config('simulator.predict_when_number_of_weeks_remains')) {
            return;
        }

        # Make predictions
        $predictions = (new Predictor($event->matchweek))->predict();

        # Save results
        TeamScore::savePredictions($predictions);
    }
}
