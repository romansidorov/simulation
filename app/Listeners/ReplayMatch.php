<?php

namespace App\Listeners;

use App\Events\MatchUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\MatchweekPlayed;

class ReplayMatch
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\MatchUpdated  $event
     * @return void
     */
    public function handle(MatchUpdated $event)
    {
        if ($event->match->wasChanged(['home_goals', 'away_goals'])) {
            # Delete related scores
            $event->match->teamScores()->delete();

            # Play match with the predefined score
            $event->match->play([
                'homeTeam' => $event->match->home_goals,
                'awayTeam' => $event->match->away_goals
            ]);

            # Refresh the matchweek results
            event(new MatchweekPlayed($event->match->matchweek));
        }
    }
}
