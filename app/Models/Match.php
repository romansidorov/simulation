<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Services\MatchPlayer;

class Match extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'matchweek_id', 'home_team_id', 'away_team_id', 'home_goals', 'away_goals', 'is_played',
    ];

    protected $casts = [
        'home_goals' => 'integer',
        'away_goals' => 'integer',
        'is_played' => 'boolean',
    ];

    /**
     * Is played scope
     */
    public function scopeIsPlayed($query)
    {
        return $query->where('is_played', 1);
    }

    /**
     * Related matchweek
     */
    public function matchweek()
    {
        return $this->belongsTo(Matchweek::class);
    }

    /**
     * Related team which played at home
     */
    public function homeTeam()
    {
        return $this->belongsTo(Team::class, 'home_team_id');
    }

    /**
     * Related team which played at away
     */
    public function awayTeam()
    {
        return $this->belongsTo(Team::class, 'away_team_id');
    }

    /**
     * Was is a draw
     */
    public function isDraw(): bool
    {
        if ($this->home_goals === $this->away_goals) {
            return true;
        }

        return false;
    }

    /**
     * Winner team
     */
    public function winner()
    {
        if ($this->isDraw()) {
            return null;
        }

        if ($this->home_goals > $this->away_goals) {
            return $this->homeTeam();
        }

        return $this->awayTeam();
    }

    /**
     * Loser team
     */
    public function loser()
    {
        if ($this->isDraw()) {
            return null;
        }

        if ($this->home_goals > $this->away_goals) {
            return $this->awayTeam();
        }

        return $this->homeTeam();
    }

    /**
     * Mark the match as played
     */
    public function markAsPlayed(): self
    {
        $this->update([
            'is_played' => 1,
        ]);

        return $this;
    }

    /**
     * Set the match score
     */
    protected function setMatchScore(array $matchScore): self
    {
        $this->update([
            'home_goals' => $matchScore['homeTeam'],
            'away_goals' => $matchScore['awayTeam'],
        ]);

        return $this;
    }

    /**
     * Set the match teams scores
     */
    protected function setTeamsScores(array $teamsScores): self
    {
        # Loop over the teams and save their scores
        foreach ($teamsScores as $teamsScoreProperties) {
            $teamsScoreProperties['team']->saveScore($teamsScoreProperties);
        }

        return $this;
    }

    /**
     * Play the match
     */
    public function play(array $matchScore = null): self
    {
        # Simulate the match playing and catch its scores
        $scores = (new MatchPlayer($this))->play($matchScore);

        $this
            ->setMatchScore($scores['matchScore'])
            ->setTeamsScores($scores['teamsScores'])
            ->markAsPlayed();

        return $this;
    }

    /**
     * Is the match played
     */
    public function isPlayed(): bool
    {
        return $this->is_played;
    }

    /**
     * Has home team won the match
     */
    public function homeWon(): bool
    {
        if (! $this->isPlayed()) {
            return false;
        }

        if ($this->isDraw()) {
            return false;
        }

        return $this->home_goals > $this->away_goals;
    }

    /**
     * Has away team won the match
     */
    public function awayWon(): bool
    {
        if (! $this->isPlayed()) {
            return false;
        }

        if ($this->isDraw()) {
            return false;
        }

        return $this->home_goals < $this->away_goals;
    }

    /**
     * Related teams scores
     */
    public function teamScores()
    {
        return $this->hasMany(TeamScore::class);
    }
}
