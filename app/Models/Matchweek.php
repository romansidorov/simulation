<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Events\MatchweekPlayed;

class Matchweek extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'is_played',
    ];

    protected $casts = [
        'is_played' => 'boolean',
    ];

    protected static function booted()
    {
        static::addGlobalScope('ordered', function (Builder $builder) {
            $builder->orderBy('id', 'asc');
        });

        static::updated(function ($matchweek) {
            # Fire an event if the matchweek is over
            if ($matchweek->wasChanged('is_played') && $matchweek->isPlayed()) {
                event(new MatchweekPlayed($matchweek));
            }
        });
    }

    /**
     * Is not played scope
     */
    public function scopeIsNotPlayed($query)
    {
        return $query->where('is_played', 0);
    }

    /**
     * Related matches
     */
    public function matches()
    {
        return $this->hasMany(Match::class);
    }

    /**
     * Mark the matchweek as played
     */
    public function markAsPlayed(): self
    {
        $this->update([
            'is_played' => 1,
        ]);

        return $this;
    }

    /**
     * Play all matches of the matchweek
     */
    public function play(): self
    {
        $this->matches->each->play();

        $this->markAsPlayed();

        return $this;
    }

    /**
     * Is the matchweek played
     */
    public function isPlayed(): bool
    {
        return $this->is_played;
    }
}
