<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Services\TeamPower;
use App\Models\{Matchweek, TeamScore};

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'stadium_capacity', 'strength',
    ];

    protected $casts = [
        'stadium_capacity' => 'integer',
        'strength' => 'integer',
    ];

    /**
     * Related scores
     */
    public function teamScores()
    {
        return $this->hasMany(TeamScore::class);
    }

    /**
     * Matches played by the team
     */
    public function playedMatches()
    {
        return Match::query()
            ->where(function($query) {
                $query
                    ->where('home_team_id', $this->getKey())
                    ->orWhere('away_team_id', $this->getKey())
                ;
            })
            ->isPlayed()
            ->get();
    }

    /**
     * Amount of the team power points just before
     * the specified match.
     * 
     * @param Match $match
     * @return int
     */
    public function powerPoints(Match $match): int
    {
        return (new TeamPower($this, $match))->getPoints();
    }

    /**
     * Save a provided score
     */
    public function saveScore(array $score): void
    {
        $lastScore = $this->teamScores->last();
        
        $this->teamScores()->create([
            'match_id' => $score['match_id'],
            'matches_played' => ($lastScore->matches_played ?? 0) + $score['matches_played'],
            'won' => ($lastScore->won ?? 0) + $score['won'],
            'drawn' => ($lastScore->drawn ?? 0) + $score['drawn'],
            'lost' => ($lastScore->lost ?? 0) + $score['lost'],
            'gf' => ($lastScore->gf ?? 0) + $score['gf'],
            'ga' => ($lastScore->ga ?? 0) + $score['ga'],
            'gd' => ($lastScore->gd ?? 0) + $score['gd'],
            'points' => ($lastScore->points ?? 0) + $score['points'],
        ]);
    }

    /**
     * Set provided prediction percentage
     * for the first non-predicted team score
     */
    public function setPrediction(int $percentage): self
    {
        $firstUnpredictedTeamScore = $this->teamScores()
            ->orderBy((new TeamScore)->getKeyName(), 'desc')
            ->first()
            ->setPrediction($percentage);

        return $this;
    }
}
