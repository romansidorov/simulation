<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Models\{Match, Matchweek, TeamScore};

class TeamScore extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'team_id', 'match_id', 'matches_played', 'won', 'drawn', 'lost', 'gf', 'ga', 'gd', 'points', 'place', 'prediction',
    ];

    protected $casts = [
        'matches_played' => 'integer',
        'won' => 'integer',
        'drawn' => 'integer',
        'lost' => 'integer',
        'won' => 'integer',
        'gf' => 'integer',
        'ga' => 'integer',
        'gd' => 'integer',
        'points' => 'integer',
        'place' => 'integer',
        'prediction' => 'integer',
    ];

    /**
     * Scope, is not predicted
     */
    public function scopeIsNotPredicted($query)
    {
        return $query->whereNull('prediction');
    }

    /**
     * Related team
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Related match
     */
    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    /**
     * Save provided predictions results
     */
    public static function savePredictions(array $predictions): void
    {
        foreach ($predictions as $prediction) {
            $prediction['team']->setPrediction($prediction['percentage']);
        }
    }

    /**
     * Set provided prediction percentage
     * for this team score slice
     */
    public function setPrediction(int $percentage): self
    {
        $this->update([
            'prediction' => $percentage,
        ]);

        return $this;
    }

    /**
     * Get the matchweek's matches teams scores
     */
    public static function getMatchweekScores(Matchweek $matchweek): Collection
    {
        # Retrieve ids of matches of the provided matchweek
        $matchweekMatchesIds = $matchweek->matches->pluck((new Match)->getKeyName());

        return TeamScore::query()
            ->whereIn('match_id', $matchweekMatchesIds)
            ->get()
            ->sortBy([
                ['points', 'desc'],
                ['won', 'desc'],
                ['drawn', 'desc'],
                ['lost', 'asc'],
                ['gf', 'desc'],
                ['ga', 'asc'],
            ])
            ->keyBy('team_id')
        ;
    }
}
