<?php

namespace App\Services;

use App\Models\{Team, TeamScore, Matchweek};
use Illuminate\Support\Collection;

class Leaderboard
{
    public Collection $teamsScores; 
    protected int $matchweeksCount;
    public int $matchesPlayed;
    protected int $totalPoints;
    public TeamScore $leader;
                   
    public function __construct(Collection $teamsScores)
    {
        $this->teamsScores = $teamsScores;
        $this->matchweeksCount = Matchweek::count();
        $this->matchesPlayed = $teamsScores->first()->matches_played;

        $this
            ->setLeader()
            ->setTotalPoints()
        ;
    }

    /**
     * Set the leader
     */
    protected function setLeader(): self
    {
        $this->leader = $this->teamsScores->first();

        return $this;
    }

    /**
     * Set total points count
     */
    protected function setTotalPoints(): void
    {
        $this->totalPoints = $this->teamsScores->sum('points');
    }

    /**
     * Get the next to specified
     */
    public function nextTo(Team $team)
    {
        # Get an offset
        $offset = $this->teamsScores->keys()->search($team->getKey(), true);

        # Get the following scores
        $remainingScores = $this->teamsScores->slice($offset + 1);

        if ($remainingScores->isNotEmpty()) {
            # Get the next
            return $remainingScores->first();
        }

        return null;
    }

    /**
     * Is there an absolute leader
     */
    public function hasAbsoluteLeader(): bool
    {
        # Get points of team which is next to the leader
        $nextToLeaderPoints = $this->nextTo($this->leader->team)->points;

        # If the leader has same points as a team which is next to the leader, then here is no an absolute leader
        if ($this->leader->points === $nextToLeaderPoints) {
            return false;
        }

        # If the leader can still catch up, he is not an absolute
        if (($this->leader->points - $nextToLeaderPoints) <= (($this->matchweeksCount - $this->matchesPlayed) * 3)) {
            return false;
        }

        # In any case here is an absolute leader of the league
        return true;
    }

    /**
     * Total points amount getter
     */
    public function getTotalPoints(): int
    {
        return $this->totalPoints;
    }

    /**
     * Decrease total points amount
     */
    public function decreaseTotalPoints(int $value): void
    {
        $this->totalPoints -= $value;
    }

    /**
     * Whether a given team is able to reach the leader
     */
    public function leaderIsUnreachableBy(Team $team): bool
    {
        # Yes, if a given team can score the required number of points for the remaining number of matches
        if (($this->leader->points - $this->teamsScores[$team->getKey()]->points) > (($this->matchweeksCount - $this->matchesPlayed) * 3)) {
            return true;
        }

        return false;
    }
}