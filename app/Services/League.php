<?php

namespace App\Services;

use App\Models\{Team, TeamScore, Match, Matchweek};
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

class League
{
    /**
     * Generating league fixtures
     */
    public function generateFixtures(): void
    {
        # Retrieving teams
        $teams = Team::query()->get();

        # Define teams count
        $teamsCount = $teams->count();

        # Define matchweeks count according to the round-robin algorithm
        $matchweeksCount = $teamsCount / 2 * ($teamsCount - 1);

        # Let's define a matches collection
        $matches = collect();

        # Iterating over matchweeks
        for ($matchweekNumber = 1; $matchweekNumber <= $matchweeksCount; $matchweekNumber++) {
            $matches = $matches->concat($this->getMatchweekMatches($matchweekNumber, $teams, $teamsCount));

            # Slide teams positions according to the round-robin algorithm
            $spliced = $teams->splice(1);
            $teams = $teams->push($spliced->pop())->concat($spliced);
        }

        # Define Matchweek class key name
        $matchweekKeyName = (new Matchweek)->getKeyName();

        # Creating matchweeks
        Matchweek::insert(array_map(fn($value) => [$matchweekKeyName => $value], range(1, $matchweeksCount)));

        # And matches
        Match::insert($matches->toArray());
    }

    /**
     * Generate and return matchweek's matches
     */
    protected function getMatchweekMatches(int $matchweekNumber, Collection $teams, int $teamsCount): object
    {
        $matches = collect();

        for ($teamIndex = 0; $teamIndex <= (($teamsCount / 2) - 1); $teamIndex++) {
            # Set home and away matches
            $match = [
                $teams[$teamIndex],
                $teams[$teamsCount - $teamIndex - 1]
            ];

            # If the match is even, swap an order for the one to be more fairy
            if ($matchweekNumber % 2 === 0) {
                $match = array_reverse($match);
            }

            $matches->push([
                'matchweek_id' => $matchweekNumber,
                'home_team_id' => $match[0]->getKey(),
                'away_team_id' => $match[1]->getKey(),
            ]);
        }

        return $matches;
    }

    /**
     * Completely reset all results and fixtures
     */
    public function reset(): void
    {
        Schema::disableForeignKeyConstraints();

        $classes = [
            TeamScore::class, Match::class, Matchweek::class,
        ];

        foreach ($classes as $class) {
            $class::truncate();
        }
    }

    /**
     * Play all matchweeks of the league
     */
    public function playAllMatchweeks(): void
    {
        # Retrieve all matchweeks
        $matchweeks = Matchweek::query()
            ->isNotPlayed()
            ->get();

        if ($matchweeks->isEmpty()) {
            return;
        }

        # Play them
        $matchweeks->each->play();
    }

    /**
     * All teams of the league
     */
    public function teams(): Collection
    {
        return Team::query()
            ->get()
            ->keyBy((new Team)->getKeyName());
    }
}