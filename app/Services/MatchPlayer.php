<?php

namespace App\Services;

use App\Models\{Team, Match};

class MatchPlayer
{
    protected Match $match;
    protected int $homeGoals = 0;
    protected int $awayGoals = 0;
    protected array $matchScore;
    protected $winner;
    protected array $points;
    protected array $teamsScores;

    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    /**
     * Play the match
     * 
     * @param array $matchScore Predefined score of the match
     * @return array
     */
    public function play(array $matchScore = null): array
    {
        $this
            ->setMatchScore($matchScore)
            ->setWinner()
            ->setPoints()
            ->setTeamsScores()
        ;

        return [
            'matchScore' => $this->matchScore,
            'teamsScores' => $this->teamsScores,
        ];
    }

    /**
     * Set the match score
     */
    protected function setMatchScore(array $matchScore = null): self
    {
        if (null === $matchScore) {
            $homeTeamPowerPoints = $this->match->homeTeam->powerPoints($this->match);
            $awayTeamPowerPoints = $this->match->awayTeam->powerPoints($this->match);

            $matchScore = [
                'homeTeam' => floor($homeTeamPowerPoints / 10),
                'awayTeam' => floor($awayTeamPowerPoints / 10)
            ];
        }

        $this->matchScore = $matchScore;

        return $this;
    }

    /**
     * Is the match drawn?
     */
    protected function isDrawn(): bool
    {
        if ($this->matchScore['homeTeam'] === $this->matchScore['awayTeam']) {
            return true;
        }

        return false;
    }

    /**
     * Set the team which won the match
     */
    protected function setWinner(): self
    {
        if ($this->isDrawn()) {
            $this->winner = null;
        }

        if ($this->matchScore['homeTeam'] > $this->matchScore['awayTeam']) {
            $this->winner = $this->match->homeTeam;
        } else {
            $this->winner = $this->match->awayTeam;
        }

        return $this;
    }

    /**
     * Set match points for teams
     */
    protected function setPoints(): self
    {
        if ($this->isDrawn()) {
            $this->points = [
                'homeTeam' => 1,
                'awayTeam' => 1,
            ];
        } else {
            $this->points = [
                'homeTeam' => $this->winner->is($this->match->homeTeam) ? 3 : 0,
                'awayTeam' => $this->winner->is($this->match->awayTeam) ? 3 : 0,
            ];
        }

        return $this;
    }

    /**
     * Set match teams scores
     */
    protected function setTeamsScores(): void
    {
        $generalScores = [
            'match_id' => $this->match->getKey(),
            'drawn' => $this->isDrawn() ? 1 : 0,
            'matches_played' => 1,
        ];

        $this->teamsScores = [
            'homeTeam' => array_merge($generalScores, $this->setHomeTeamScore()),
            'awayTeam' => array_merge($generalScores, $this->setAwayTeamScore()),
        ];
    }

    /**
     * Set match score for the home team
     */
    protected function setHomeTeamScore(): array
    {
        $isWinner = $this->winner->is($this->match->homeTeam);

        return [
            'team' => $this->match->homeTeam,
            'won' => ! $this->isDrawn() && $isWinner ? 1 : 0,
            'lost' => ! $this->isDrawn() && ! $isWinner ? 1 : 0,
            'gf' => $this->matchScore['homeTeam'],
            'ga' => $this->matchScore['awayTeam'],
            'gd' => $this->matchScore['homeTeam'] - $this->matchScore['awayTeam'],
            'points' => $this->points['homeTeam'],
        ];
    }

    /**
     * Set match score for the away team
     */
    protected function setAwayTeamScore(): array
    {
        $isWinner = $this->winner->is($this->match->awayTeam);

        return [
            'team' => $this->match->awayTeam,
            'won' => ! $this->isDrawn() && $isWinner ? 1 : 0,
            'lost' => ! $this->isDrawn() && ! $isWinner ? 1 : 0,
            'gf' => $this->matchScore['awayTeam'],
            'ga' => $this->matchScore['homeTeam'],
            'gd' => $this->matchScore['awayTeam'] - $this->matchScore['homeTeam'],
            'points' => $this->points['awayTeam'],
        ];
    }
}