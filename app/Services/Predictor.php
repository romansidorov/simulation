<?php

namespace App\Services;

use App\Models\{Team, Match, Matchweek, TeamScore};
use Illuminate\Support\Collection;
use App\Services\Leaderboard;

class Predictor
{
    protected Matchweek $matchweek;
    protected array $results;
    protected Leaderboard $leaderBoard;

    public function __construct(Matchweek $matchweek)
    {
        $this->matchweek = $matchweek;

        $this->setLeaderboard();
    }

    /**
     * Predict based on played matches
     */
    public function predict(): array
    {
        $predictions = $this->leaderboard->teamsScores
            ->reverse() # Start calculating the odds from weak teams to strong teams to account for weak teams dropping out of the overall probability when there are no odds
            ->map(function($teamScore) {
                return [
                    'team' => $teamScore->team,
                    'percentage' => $this->calculateTeamOdds($teamScore->team),
                ];
            })
            ->toArray();

        return $predictions;
    }

    /**
     * Setting the last saved teams scores at a convinient form
     */
    protected function setLeaderboard(): void
    {
        # Retrieve the matchweek's teams scores
        $this->teamsScores = TeamScore::getMatchweekScores($this->matchweek);

        # Set the current leaderboard
        $this->leaderboard = new Leaderboard($this->teamsScores);
    }

    /**
     * What is the chance of the team to win the league at the moment
     */
    protected function calculateTeamOdds(Team $team): int
    {
        /**
         * If this is the only team in the lead and this team has as many points
         * as no one else can score, then its odds are 100%
         */
        if ($this->leaderboard->hasAbsoluteLeader() && $this->leaderboard->leader->team_id === $team->getKey()) {
            return 100;
        }

        /**
         * If there are not enough matches to reach
         * the leader, odds are 0
         */
        if ($this->leaderboard->hasAbsoluteLeader() && $this->leaderboard->leader->team_id !== $team->getKey()) {
            return 0;
        }

        # Get a givet team score
        $teamScore = $this->teamsScores[$team->getKey()];

        # Calculate other cases
        if (! $this->leaderboard->hasAbsoluteLeader() && $this->leaderboard->leaderIsUnreachableBy($team)) {
            $this->leaderboard->decreaseTotalPoints($teamScore->points);

            return 0;
        }

        return round($teamScore->points / $this->leaderboard->getTotalPoints(), 2) * 100;
    }
}