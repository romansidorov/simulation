<?php

namespace App\Services;

use App\Models\{Team, Match};

class TeamPower
{
    protected Team $team;
    protected Match $match;
    protected int $points = 0;

    const FORM_EACH_WON_MATCH_POINTS = 3;
    const MOTIVATION_IF_LAST_MATCH_WON = 5;
    const SUPPORT_QUALITY_POINTS = 3;

    public function __construct(Team $team, Match $match)
    {
        $this->team = $team;
        $this->match = $match;
    }

    /**
     * Get current points value
     */
    public function getPoints(): int
    {
        $this->calculate();

        return $this->points;
    }

    /**
     * Increase points value
     */
    protected function addPoints(int $value): void
    {
        $this->points += $value;
    }

    /**
     * Calculate total points based on it's dependings
     */
    protected function calculate(): void
    {
        $this
            ->setStrength()
            ->setForm()
            ->setMotivation()
            ->setSupportQuality()
            ->setSupportQuantity()
            ->setLuck()
        ;
    }

    /**
     * Set strength points. It depends on
     * the team (predefined) strength in the season.
     * 
     * This factor could be really influable
     * when the match plays between two teams with different strenghts
     */
    protected function setStrength(): self
    {
        # Retrieving the team strength
        $strength = round($this->team->strength / 10);

        $this->addPoints($strength);

        return $this;
    }

    /**
     * Set form points. It depends on
     * the current team form
     */
    protected function setForm(): self
    {
        # Retrieving matches played by the team and count those amount
        $wonMatchesCount = $this->team->playedMatches()
            ->filter(function($match) {
                if (! $match->isDraw() && $match->winner()->is($this->team)) {
                    return true;
                }

                return false;
            })
            ->count();

        $this->addPoints($wonMatchesCount * self::FORM_EACH_WON_MATCH_POINTS);

        return $this;
    }

    /**
     * Set motivation points. The motivation depends on
     * previously played match won or not
     */
    protected function setMotivation(): self
    {
        # Retrieving matches played by the team
        $playedMatches = $this->team->playedMatches();

        if ($playedMatches->isNotEmpty()) {
            # Add points when the last match won
            $lastPlayedMatch = $playedMatches->last();

            if (! $lastPlayedMatch->isDraw() && $lastPlayedMatch->winner()->is($this->team)) {
                $this->addPoints(self::MOTIVATION_IF_LAST_MATCH_WON);
            }
        }

        return $this;
    }

    /**
     * Set quality support points. It depends on
     * the team plays at home or away
     */
    protected function setSupportQuality(): self
    {
        if ($this->match->homeTeam->is($this->team)) {
            $this->addPoints(self::SUPPORT_QUALITY_POINTS);
        }

        return $this;
    }

    /**
     * Set quantity support points. It depends on
     * the team supporters count on the match.
     */
    protected function setSupportQuantity(): self
    {
        # Retrieving the match stadium capacity
        $stadiumCapacity = $this->match->homeTeam->stadium_capacity;

        # Calculating supporters percentage
        $supportersPercentage = $this->match->homeTeam->is($this->team)
            ? 0.7
            : 0.3
        ;

        $supportQuantity = floor($stadiumCapacity * $supportersPercentage * 0.00005);

        $this->addPoints($supportQuantity);

        return $this;
    }

    /**
     * Set the luck factor
     */
    protected function setLuck(): self
    {
        $luckCases = [
            0.9, 0.7, 0.85, 0.95, 0.6,
            1.05, 1.3, 1.0, 1.1, 1.0,
        ];

        $luck = $luckCases[rand(0, count($luckCases) - 1)];

        $this->points = round($this->points * $luck);

        return $this;
    }
}