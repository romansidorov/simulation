<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->tinyIncrements('id');
            
            $table->unsignedTinyInteger('matchweek_id');
            $table
                ->foreign('matchweek_id')
                ->references('id')
                ->on('matchweeks')
                ->cascadeOnDelete();

            $table->unsignedTinyInteger('home_team_id');
            $table
                ->foreign('home_team_id')
                ->references('id')
                ->on('teams')
                ->cascadeOnDelete();

            $table->unsignedTinyInteger('away_team_id');
            $table
                ->foreign('away_team_id')
                ->references('id')
                ->on('teams')
                ->cascadeOnDelete();

            $table->unsignedTinyInteger('home_goals')->default(0);
            $table->unsignedTinyInteger('away_goals')->default(0);

            $table->boolean('is_played')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
