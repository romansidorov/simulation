<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_scores', function (Blueprint $table) {
            $table->tinyIncrements('id');
            
            $table->unsignedTinyInteger('team_id');
            $table
                ->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->cascadeOnDelete();

            $table->unsignedTinyInteger('match_id');
            $table
                ->foreign('match_id')
                ->references('id')
                ->on('matches')
                ->comment('The match by the results of which these scores were calculated.')
                ->cascadeOnDelete();

            $table->unsignedTinyInteger('matches_played');
            $table->unsignedTinyInteger('won');
            $table->unsignedTinyInteger('drawn');
            $table->unsignedTinyInteger('lost');
            $table->unsignedTinyInteger('gf');
            $table->unsignedTinyInteger('ga');
            $table->tinyInteger('gd');
            $table->unsignedTinyInteger('points');
            $table->unsignedTinyInteger('prediction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_scores');
    }
}
