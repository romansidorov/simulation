<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            // ['name' => 'Liverpool', 'stadium_capacity' => 53394, 'strength' => 175, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t14@x2.png'],
            ['name' => 'Manchester City', 'stadium_capacity' => 60260, 'strength' => 171, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t43@x2.png'],
            ['name' => 'Chelsea', 'stadium_capacity' => 40853, 'strength' => 171, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t8@x2.png'],
            ['name' => 'Arsenal', 'stadium_capacity' => 60260, 'strength' => 175, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t3@x2.png'],
            ['name' => 'Aston Villa', 'stadium_capacity' => 42682, 'strength' => 122, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t7@x2.png'],
            // ['name' => 'Leeds United', 'stadium_capacity' => 37890, 'strength' => 138, 'logo' => 'https://resources.premierleague.com/premierleague/badges/25/t2@x2.png'],
        ];

        Team::insert($teams);
    }
}
