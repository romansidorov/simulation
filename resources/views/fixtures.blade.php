@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Fixtures') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row mb-3">
                        @foreach ($matchweeks as $matchweek)
                            <div class="col-3 mb-3">
                                <table class="table">
                                    <thead class="table-light">
                                        <tr>
                                            <th scope="col" colspan="3">Week {{ $matchweek->getKey() }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($matchweek->matches as $match)
                                            <tr is="match"
                                                :match="{{ $match }}"
                                                :teams="{{ $teams }}"
                                                match-update-route="{{ route('matches.update', $match) }}"
                                                able-to-change="false"
                                            ></tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>

                    @if (request()->has('all'))
                        <a href="{{ route('matchweek', $matchweeks->last()) }}" class="btn btn-warning">Go back</a>
                    @else
                        <a href="{{ route('simulation') }}" class="btn btn-primary">Start simulation</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
