@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Simulation') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <matchweek
                        :teams-scores="{{ $teamsScores->values() }}"
                        :teams="{{ $teams }}"
                        :matchweek="{{ $matchweek }}"
                        :matches="{{ $matches }}"
                    ></matchweek>

                    <a href="{{ route('play-all') }}" class="btn btn-primary">Play the rest of weeks</a>
                    <a href="{{ route('play-next') }}" class="btn btn-primary">Play next week</a>
                    <a href="{{ route('reset') }}" class="btn btn-danger">Reset</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
