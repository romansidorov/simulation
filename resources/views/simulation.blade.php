@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Simulation') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-8 mb-3">
                            <table class="table">
                                <thead class="table-light">
                                    <tr class="text-center">
                                        <th scope="col" class="text-start">Team name</th>
                                        <th scope="col">Played</th>
                                        <th scope="col">W</th>
                                        <th scope="col">D</th>
                                        <th scope="col">L</th>
                                        <th scope="col">GF</th>
                                        <th scope="col">GA</th>
                                        <th scope="col">GD</th>
                                        <th scope="col">Points</th>
                                        <th scope="col">Odds, %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($teams as $team)
                                        <tr class="text-center">
                                            <td class="text-start">
                                                <img src="{{ $team->logo }}" alt="" class="team-logo">
                                                {{ $team->name }}
                                            </td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td class="table-light">0</td>
                                            <td class="table-primary">0</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-4 mb-3">
                            <table class="table">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col" colspan="3">Week 1</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($matches as $match)
                                        <tr>
                                            <td>
                                                <img src="{{ $teams[$match->home_team_id]->logo }}" alt="" class="team-logo">
                                                {{ $teams[$match->home_team_id]->name }}
                                            </td>
                                            <td>–</td>
                                            <td>
                                                <img src="{{ $teams[$match->away_team_id]->logo }}" alt="" class="team-logo">
                                                {{ $teams[$match->away_team_id]->name }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <a href="{{ route('play-all') }}" class="btn btn-primary">Play all weeks</a>
                    <a href="{{ route('play-next') }}" class="btn btn-primary">Play next week</a>
                    <a href="{{ route('reset') }}" class="btn btn-danger">Reset</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
