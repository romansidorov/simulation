@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Teams list') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <tr class="text-center">
                            <th scope="col" class="text-start">Name</th>
                            <th scope="col">Strength</th>
                            <th scope="col">Stadium capacity</th>
                        </tr>

                        @foreach ($teams as $team)
                            <tr class="text-center">
                                <td class="text-start">
                                    <img src="{{ $team->logo }}" alt="" class="team-logo">
                                    {{ $team->name }}
                                </td>
                                <td>{{ $team->strength }}</td>
                                <td>{{ number_format($team->stadium_capacity, 0, ',', ' ') }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <a href="{{ route('generate-fixtures') }}" class="btn btn-primary">Generate fixtures</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
