<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/teams');

Route::get('/teams', [AppController::class, 'teams'])->name('teams');

Route::get('/fixtures/generate', [AppController::class, 'generateFixtures'])->name('generate-fixtures');
Route::get('/fixtures', [AppController::class, 'fixtures'])->name('fixtures');

Route::get('/simulation', [AppController::class, 'simulation'])->name('simulation');
Route::get('/reset', [AppController::class, 'reset'])->name('reset');
Route::get('/play-all', [AppController::class, 'playAllWeeks'])->name('play-all');
Route::get('/play-next', [AppController::class, 'playNextWeek'])->name('play-next');

Route::get('/matchweeks/{matchweek}/scores', [AppController::class, 'matchweekScores'])->name('matchweeks.scores');
Route::get('/matchweeks/{matchweek}', [AppController::class, 'matchweek'])->name('matchweek');
Route::put('/matches/{match}', [AppController::class, 'matchUpdate'])->name('matches.update');